<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login/index');
});

Route::get('/registernew', function () {
    return view('register/index');
});

Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail');
Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm');
Route::post('/password/reset', 'ResetPasswordController@reset');
Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm');
Route::get('/session/{jwt}/{data}', 'Session@index');
Route::get('/logout', 'Session@logout');



Route::group(['middleware' => 'checksess'],function(){
    Route::get('/dashboard', function () {
            return view('dashboard/index');
        });
    });
// Route::get('/dashboard', function () {
//     return view('dashboard/index');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
