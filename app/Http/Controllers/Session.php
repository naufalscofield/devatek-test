<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class Session extends Controller
{
    public function index(Request $request, $jwt, $data)
    {
        $request->session()->put('jwt',$jwt);
        $request->session()->put('data',$data);
        return redirect('/dashboard');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }
}
?>