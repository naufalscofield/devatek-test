@extends('login/master')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(/Login_v15/images/bg-01.jpg);">
					<span class="login100-form-title-1">
						Login
					</span>
				</div>

				<form class="login100-form validate-form" onsubmit="login(); return false;">
					<div class="wrap-input100 validate-input m-b-26" data-validate="Email is required">
						<span class="label-input100">Email</span>
						<input class="input100" type="email" id="email" placeholder="Enter Email">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" id="password" placeholder="Enter password">
						<span class="focus-input100"></span>
					</div>

					<div class="flex-sb-m w-full p-b-30">
						<div class="contact100-form-checkbox">
							<a href="/registernew" class="txt1">
								Register
							</a>
						</div>

						<div>
							<a href="/password/reset" class="txt1">
								Forgot Password?
							</a>
						</div>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script language="javascript">

		function login(){
			var email        		= $('#email').val();
			var password          	= $('#password').val();

			// console.log(email,password)

			$.post("http://localhost:9000/login", {
				email: email,
				password: password
			}, function(data, status){
				console.log(data)
				if (data.status == true)  {
					toastr.success(data.message)
					var jwt = data.data.token
					var data = data.data.user
					window.location.href="/session/" + jwt + "/" + data
				}
				else {
					console.log(data)
					toastr.error(data.message)
				}
			});
		}

	</script>
@endsection