@extends('register/master')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<!--===============================================================================================-->	
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(/Login_v15/images/SAT_Registration.jpeg);">
					<span class="login100-form-title-1">
						Register
					</span>
				</div>

				<form class="login100-form validate-form" onsubmit="register(); return false;">
					<div class="wrap-input100 validate-input m-b-26" data-validate="Email is required">
						<span class="label-input100">Email</span>
						<input class="input100" type="email" id="email" placeholder="Enter Email">
						<span class="focus-input100"></span>
					</div>
					
					<div class="wrap-input100 validate-input m-b-26" data-validate="No Telp is required">
						<span class="label-input100">No Telp</span>
						<input class="input100" type="text" id="no_telp" placeholder="Enter No Telp">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" id="password" placeholder="Enter password">
						<span class="focus-input100"></span>
					</div>

					<div class="flex-sb-m w-full p-b-30">
						<div class="contact100-form-checkbox">
							<a href="/" class="txt1">
								Login
							</a>
						</div>

					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Register
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script language="javascript">

		function register(){
			var email        		= $('#email').val();
			var no_telp        		= $('#no_telp').val();
			var password          	= $('#password').val();

			$.post("http://localhost:9000/register", {
				email: email,
				no_telp: no_telp,
				password: password
			}, function(data, status){
				console.log(data)
				if (data.status) {
					toastr.success(data.message)
					window.location.href="/"
				}
				else {
					toastr.error(data.message)
				}
			});
		}

	</script>
@endsection